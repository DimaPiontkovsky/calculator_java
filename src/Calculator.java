import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calculator implements App {
    @Override
    public void run() {

        BufferedReader readerCalc = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите первое число: ");
            int firstNumber = Integer.parseInt(readerCalc.readLine());
            System.out.println("Введите второе число: ");
            int secondNumber = Integer.parseInt(readerCalc.readLine());
            System.out.println("Введите арефметическую операцию: +, -. * или / ");
            String sign = readerCalc.readLine();
            int result = 0;
            switch (sign) {
                case "+" -> result = firstNumber + secondNumber;
                case "-" -> result = firstNumber - secondNumber;
                case "*" -> result = firstNumber * secondNumber;
                case "/" -> result = firstNumber / secondNumber;
                default -> System.out.println("Не верная операция" + "\n");
            }
            System.out.println("Результат: " + result + "\n");
        } catch (IOException | ArithmeticException e) {                                   //fix ArithmeticException
            System.out.println("Ошибочный ввод" + "\n");
        }
    }
}