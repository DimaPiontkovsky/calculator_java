public class MyFactory {
    public App create(String typeOfApp){
        return switch (typeOfApp) {
            case "Calculator" -> new Calculator();
            case "Converter" -> new Converter();
            default -> null;
        };
    }
}
