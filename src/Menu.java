import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Menu {
    public void menuChoice() {
        MyFactory myFactory = new MyFactory();
        App calculator = myFactory.create("Calculator");
        App converter = myFactory.create("Converter");
        BufferedReader menuReader = new BufferedReader(new InputStreamReader(System.in));
        try {                                                                              //fix exception
            System.out.println("Сделайте выбор:"
                    + "\n"
                    + "1. Калькулятор"
                    + "\n"
                    + "2. Конвертер");

            int choiceApp = Integer.parseInt(menuReader.readLine());
            switch (choiceApp) {
                case 1 -> calculator.run();
                case 2 -> converter.run();
                default -> System.out.println("Вы сделали не правильный выбор" + "\n");
            }
        } catch (IOException | NumberFormatException e) {                                  //fix NumberFormatException
            System.out.println("Ошибочный ввод" + "\n");
        }
        menuChoice();                                                                      //fix exit after calculation
    }
}
