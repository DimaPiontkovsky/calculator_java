public class Main {

    public static void main(String[] args) {
        SomeClass someClass = new SomeClass();
        MyClass myClass = new MyClass();
        Menu menu = new Menu();
        menu.menuChoice();
        someClass.registerCallBack(myClass);
        someClass.doSomething();
    }

}