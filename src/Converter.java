import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Converter implements App {
    @Override
    public void run() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Сделайте выбор: "
                    + "\n"
                    + "1. Километры/час в мили/час"
                    + "\n"
                    + "2. Граммы в киллограммы");
            int selectConvert = Integer.parseInt(bufferedReader.readLine());
            switch (selectConvert) {
                case 1 -> {
                    System.out.println("Введите скорость в километрах: ");
                    double kilometers = Double.parseDouble(bufferedReader.readLine());
                    double mile = kilometers / 1.61;
                    System.out.println("Скорость составит: " + mile + " миль в час" + "\n");
                }
                case 2 -> {
                    System.out.println("Введите граммы: ");
                    double gram = Double.parseDouble(bufferedReader.readLine()); //fix type
                    double kilogram = gram / 1000.000;
                    System.out.println("Вес в киллограммах составит: " + kilogram + " кг" + "\n");
                }
                default -> System.out.println("Не правильная операция" + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}